# Recipe App Api Proxy

NGINX Proxy For Our App

## USAGE

### Environment Variables

    - `LISTEN_PORT` - Port NGINX server listens on (default: `8000`)
    - `APP_HOST` - Hostname of app where the proxy should forward the requests to. (default: `app`)
    - `APP_PORT` - Port of the app to forward requests to. (default: `9000`)
